import { config, cdnBase } from '../../config/index';

/** 获取首页数据 */
function mockFetchHome() {
  const { delay } = require('../_utils/delay');
  const { genSwiperImageList } = require('../../model/swiper');
  return delay().then(() => {
    return {
      swiper: genSwiperImageList(),
      newsCateArr: [{ val: 1, label: '上门代厨' }, { val: 2, label: '婚宴到家' }, { val: 3, label: '保洁清洗' }, { val: 4, label: '保姆月嫂' }, { val: 5, label: '家庭维修' }, { val: 6, label: '搬家安装' }],
      tabList: [
        {
          text: '人气榜',
          key: 0,
        },
        {
          text: '好评榜',
          key: 1,
        },
      ]
    };
  });
}

/** 获取首页数据 */
export function fetchHome() {
  if (config.useMock) {
    return mockFetchHome();
  }
  return new Promise((resolve) => {
    resolve('real api');
  });
}
